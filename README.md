# Description : 

	Dans le cadre de cet exercice, le concept de notre outil de librairie à été reprise dans une version allégée.
	Cette librairie permet à nos experts de chercher des hotels pouvant correspondre aux besoins de nos clients.

	Cet exercice s'articule en deux parties : 
		- Analyser le code actuel puis le rendre fonctionnel
		- Intégrer le style des cartes pour reproduire celui de notre outil

## Etape I :

	Cette première étape consiste à vous familiariser avec le code déjà fournis et de le rendre opérationnel.
	Pour cela, il est recommandé d'être familier avec vuejs ainsi que son store nommé vuex.

	Les fichiers autres que ceux indiqués ci-dessous ne doivent pas êtres modifiés
	Les parties devant être complétés sont indiqué par le commentaire "// A compléter", les autres parties de doivent pas être modifiés

	Les objectifs sont les suivants : 
		- Afficher les composants ExperienceCard et HotelCard suivant la selection de l'utilisateur
		- Les cartes affichés doivent respecter les filtres selectionnés par l'utilisateur
			- Un filtre ayant les valeur par défaut ([min, max]) est considéré comme inactif.
			- Si tous les filtres sont inactifs, toutes les cartes doivent être affichés.
		- Le filtrage des résultats dois se faire au niveau du store

	Fichiers concernés : 
		- pages/index.vue
		- store/search.js

	Temps estimé : 30 à 90 minutes.

## Etape II : 

	Cette seconde étape consiste à faire l'intégration des deux types de cartes. HTML5/SCSS(CSS3) inclus.
	La maquette des deux cartes se trouves dans le dossier "./maquettes/"

	Les fichiers autres que ceux indiqués ci-dessous ne doivent pas êtres modifiés
	Les contenus de <template> et <style> peuvent être modifiés; Le contenu de la balise <script> ne peux pas être modifié.
	Le "pixel-perfect" n'est pas recherché, la qualité de la structure et des règles scss sera privilégié sur l'exactitude.

	Fichiers concernés : 
		- components/ExperienceCard.vue
		- components/HotelCard.vue

	temps estimé : 20 à 40 minutes.


# pe_trial

> Trial project for Prochaine Escale candidates

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

