
import _ from 'lodash';
import moment from 'moment';
import faker from 'faker';

// Initialisation des données d'exemple
const count_hotel = faker.random.number({min: 20, max: 60});
const count_experience = faker.random.number({min: 20, max: 60});

const list = {
	hotel: (new Array(count_hotel).fill(null)).map(() => ({
		id: faker.random.uuid(),
		name: faker.company.companyName(),
		subtitle: faker.company.catchPhrase(),
		price: faker.random.number({min: 1, max: 150}),
		capacity: faker.random.number({min: 1, max: 150}),
		date_create: moment(faker.date.recent()),
		destination: faker.address.country(),
		img: `https://picsum.photos/seed/${faker.random.uuid()}/400/400`,
		author: {
			img: faker.internet.avatar(),
			name: `${faker.name.firstName()} ${faker.name.lastName()}`,
		},

		room_count: faker.random.number({min: 1, max: 300})
	})),
	experience: (new Array(count_experience).fill(null)).map(() => ({
		id: faker.random.uuid(),
		name: faker.company.companyName(),
		subtitle: faker.company.catchPhrase(),
		price: faker.random.number({min: 1, max: 150}),
		capacity: faker.random.number({min: 1, max: 150}),
		date_create: moment(faker.date.recent()),
		destination: faker.address.country(),
		img: `https://picsum.photos/seed/${faker.random.uuid()}/400/400`,
		author: {
			img: faker.internet.avatar(),
			name: `${faker.name.firstName()} ${faker.name.lastName()}`,
		},

		duration: faker.random.number({min: 1, max: 10})
	}))
};

const getDatas = (type) => Promise.resolve(list[type])



export const state = () => ({
	results: null
})

export const mutations = {
	setResults(state, {results}) {
		state.results = results;
	}
}

export const actions = {
	startSearch({commit}, {type, filters}) {

		// A compléter

		getDatas(type)
			.then((results) => {
				console.log('Résultats non filtrés : ', results);
			})

	}
}

export const getters = {
	results: (state) => state.results,
}